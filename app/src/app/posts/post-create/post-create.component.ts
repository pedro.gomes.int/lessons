// import { Component, EventEmitter, Output } from '@angular/core';
import { PostsService } from './../posts.service';
import { Component } from '@angular/core';
import { NgForm } from "@angular/forms";

import { Post } from "../post.model";


@Component({
    selector: 'app-post-create',
    templateUrl: './post-create.component.html',
    styleUrls: [ './post-create.component.css' ]
})

export class PostCreateComponent {

    constructor(public postsService: PostsService) {}

    onAddPost(form: NgForm){
        if(form.invalid){
            return;
        }
        // const post: Post = {
        //     title: form.value.title,
        //     authorName: form.value.authorName,
        //     content: form.value.content
        // };

        this.postsService.addPost(form.value.title, form.value.authorName, form.value.content);
        form.resetForm();
    }

}



// export class PostCreateComponent {

//     enteredTitle = '';
//     enteredAuthorName = '';
//     enteredContent = '';

//     @Output() postCreated = new EventEmitter<Post>();

//     onAddPost(form: NgForm) {
//         if(form.invalid){
//             return;
//         }
//         const post: Post = {
//             title: form.value.title,
//             authorName: form.value.authorName,
//             content: form.value.content
//         };
//         this.postCreated.emit(post);
//     }
// }
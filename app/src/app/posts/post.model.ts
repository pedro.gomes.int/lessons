export interface Post{
    title: string;
    authorName: string;
    content: string;
}